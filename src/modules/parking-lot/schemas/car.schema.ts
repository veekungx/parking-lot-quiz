import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { CarSize } from '../../../models/car';

@Schema()
export class CarModel extends Document {
  @Prop()
  plateNumber: string;

  @Prop()
  carSize: CarSize;
}

export const CarSchema = SchemaFactory.createForClass(CarModel);

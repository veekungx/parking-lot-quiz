import { Document } from 'mongoose';
import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';
import { CarModel } from './car.schema';

@Schema()
export class ParkingLotModel extends Document {
  // @Prop()
  // slots: Map<number, CarModel>;

  @Prop()
  name: string;
}

export const ParkingLotSchema = SchemaFactory.createForClass(ParkingLotModel);

import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ParkingLot } from '../../../models/parking-lot';
import { ParkingLotHasBeenCreatedError } from '../../../errors/parking-lot-has-been-created.error';
import { ParkingLotNotExistsError } from '../../../errors/parking-lot-not-exists.error';
import { ParkingLotModel } from '../schemas/parking-lot.schema';
import { CarModel } from '../schemas/car.schema';
import { Car } from '../../../models/car';

@Injectable()
export class ParkingLotRepository {
  private parkingLot: ParkingLot;

  constructor(
    @InjectModel(ParkingLotModel.name)
    private parkingLotModel: Model<ParkingLotModel>,
    @InjectModel(CarModel.name)
    private carModel: Model<CarModel>,
  ) {}

  async get(): Promise<ParkingLot> {
    if (!this.parkingLot) {
      throw new ParkingLotNotExistsError();
    }
    return this.parkingLot;
  }

  async create(): Promise<ParkingLot> {
    if (this.parkingLot) {
      throw new ParkingLotHasBeenCreatedError();
    }
    this.parkingLot = new ParkingLot();
    return this.parkingLot;
  }

  async save(parkingLot: ParkingLot): Promise<void> {
    const p = new this.parkingLotModel({ name: 'hello' });

    // parkingLot.slotMap.forEach((car: Car, slotId: number) => {
    //   if (slotId === null) {
    //     p.slots.set(slotId, null);
    //   } else {
    //     const newCar = new this.carModel({
    //       plateNumber: car.getPlateNumber(),
    //       carSize: car.getCarSize(),
    //     });
    //     p.slots.set(slotId, newCar);
    //   }
    // });

    await p.save();
  }
}
